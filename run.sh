#!/bin/bash 
if [ "$#" -ne 3 ]; then
	VM_MEM=4096
	if [ "$#" -ne 2 ]; then
		echo "usage: $0 GITLAB_COORDINATOR_URL GITLAB_TOKEN VM_MEMORY(OPTIONAL)"
		exit 2
	fi
else
	VM_MEM=$3
fi

set -e
printf "\n\nProvisioning integration-vm...\n\n"

cd integration-vm
GITLAB_COORDINATOR_URL=$1 GITLAB_TOKEN=$2 VM_RAM_AVAILABLE=$VM_MEM vagrant up

printf "\n\nProvisioning integration-vm... Done!"
printf "\n\nProvisioning stage-vm...\n\n"

cd ../stage-vm
GITLAB_COORDINATOR_URL=$1 GITLAB_TOKEN=$2 VM_RAM_AVAILABLE=$VM_MEM vagrant up

printf "\n\nProvisioning stage-vm... Done!"
printf "\n\nProvisioning prod-vm...\n\n"

cd ../prod-vm
GITLAB_COORDINATOR_URL=$1 GITLAB_TOKEN=$2 VM_RAM_AVAILABLE=$VM_MEM vagrant up

printf "\n\nProvisioning prod-vm... Done!\n\n"