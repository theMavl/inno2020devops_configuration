# -*- mode: ruby -*-
#
# vi: set ft=ruby :
# vim: ts=2

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
 
  config.vm.box = "ubuntu/xenial64"
  config.vm.hostname = "prod-vm"
  config.vm.box_download_insecure = false 
  
  ENV['LC_ALL']="en_US.UTF-8"
  

  config.vm.network :private_network, ip: "192.168.33.99"

  vm_ram_available = ENV['VM_RAM_AVAILABLE']

  config.vm.provider :virtualbox do |vb|
   
    vb.customize [
      'modifyvm', :id,
      '--natdnshostresolver1', 'on',
      '--memory', vm_ram_available,
      '--cpus', '2'
    ]
    
  end  
  
  config.vm.provision "shell", inline: <<-SHELL
     	sudo apt-get update
     	sudo apt-get install --yes python
   	SHELL

  gitlab_coordinator_url = ENV['GITLAB_COORDINATOR_URL']
  gitlab_token = ENV['GITLAB_TOKEN']
   
  config.vm.provision "ansible" do |ansible|
    	ansible.playbook = "./playbook/playbook.yml"
      ansible.extra_vars = {
        gitlab_runner_coordinator_url: gitlab_coordinator_url,
        gitlab_runner_registration_token: gitlab_token
      }
  end
  
end
